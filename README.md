# Translately

A bash script that will translate .txt or .md to any language, **while keeping the original formatting**(when tranlating .md files).



* Running the application, you will be asked to select a plain text file to translate:

![image 1](screenshot1.png)

* After the file selection, you will be asked to select the language to translate to:

![image 2](screenshot2.png)

* Now the process begins:

![image 3](screenshot3.png)

* The application is translating  the text line by line:

![image 3](screenshot4.png)

* You get a last message when the text is complete:

![image 3](screenshot5.png)

* In the case of translation of an .md file, the **FORMATTING OF THE TEXT IS MAINTAINED IN THE TRANSLATED TEXT**.

![image 0](screenshot0.png)


 It uses the **Translate Shell** command-line translator (https://www.soimort.org/translate-shell/).
 
## INSTALLATION INSTRUCTIONS

  * Change directory to translately:
  
    cd  translately

  * Make  translately.sh executable:
  
      chmod +x translately.sh 

## DEPENDENCIES



  * Zenity:

    sudo apt install zenity




  * <u>Install trans from git:</u>

    $ git clone https://github.com/soimort/translate-shell

    $ cd translate-shell/

    $ make

    $ [sudo] make install

## LANGUAGES

You can enrich the languages selection menu ofthe application, by editing the  translately .sh script, adding the languages you prefer.
The line to edit in translately.sh is line 19. 
There you can add 

    False [Language-name] [Language-code]

Language codes are described  in translate-shell repository page: 
[https://github.com/soimort/translate-shell/wiki/Languages](https://github.com/soimort/translate-shell/wiki/Languages)







## RUN

    ~/translately/translately.sh

